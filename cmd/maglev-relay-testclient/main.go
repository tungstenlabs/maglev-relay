package main

import (
	"context"
	"fmt"
	"maglev/auth"
	"maglev/internal/cryptov1/signing"
	"maglev/transport"
	"maglev/transport/handshake"
	"maglev/transport/websocket"
	"os"
	"strings"

	"go.uber.org/zap"
)

func main() {
	logger, err := zap.NewDevelopment()
	if err != nil {
		zap.S().Fatal("error setting up logger: ", err)
	}
	log := logger.Sugar()
	defer log.Sync()

	if len(os.Args) > 3 {
		log.Fatalf("Bad args: %v", os.Args[1:])
	}

	baseURL := "localhost:3035"
	if len(os.Args) > 1 && os.Args[1] != "" {
		baseURL = os.Args[1]
	}

	if !strings.Contains(baseURL, "//") {
		baseURL = fmt.Sprintf("http://%s", baseURL)
	}
	log.Info("Base URL: ", baseURL)

	key, err := signing.GenerateKey()
	if err != nil {
		log.Fatal("Failed to generate key: ", err)
	}

	nodeID, _ := auth.NodeIDFromPublicKey(key.Public())
	log.Info("Node ID: ", nodeID)

	// Connect WS
	log.Infof("Connecting to %s...", baseURL)
	ctx := context.Background()
	wsCfg := &websocket.Config{Logger: log}
	wsURL := strings.Replace(baseURL, "http", "ws", 1) + "/v1/relay"
	t, err := wsCfg.Dial(ctx, wsURL)
	if err != nil {
		log.Fatal("Dial error: ", err)
	}
	defer t.Close(nil)

	// Handshake
	hsRes, err := handshake.Config{
		PrivateKey: key,
	}.Perform(t)
	if err != nil {
		log.Fatal("Handshake error: ", err)
	}
	log.Infof("Handshake result: %+v", hsRes)

	// Send msg (maybe)
	if len(os.Args) > 2 {
		msg := &transport.Msg{}
		msg.DestNodeId = os.Args[2]
		if len(os.Args) > 3 {
			msg.Payload = []byte(os.Args[3])
		}
		log.Infof("Sending: %s payload:%+q", &msg.MsgHeader, msg.Payload)
		t.WriteMsg(msg)
	}

	// Read loop
	msg := &transport.Msg{}
	for {
		if err := t.ReadMsg(msg); err != nil {
			log.Fatal("ReadMsg failed: ", err)
			break
		}
		log.Infof("Received: %s payload:%+q", &msg.MsgHeader, msg.Payload)
	}
}
