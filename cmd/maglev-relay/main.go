package main

import (
	"encoding/base64"
	"flag"
	"net/http"

	"go.uber.org/zap"

	"maglev/internal/cryptov1/signing"
	"maglev/router"
	"maglev/transport/websocket"
)

var (
	addr = flag.String("addr", "localhost:3035", "http listen address")
)

func main() {
	flag.Parse()

	logger, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}
	log := logger.Sugar()
	defer log.Sync()

	// Configure relay
	relayKey, err := signing.GenerateKey()
	if err != nil {
		log.Fatal("GenerateKey failed:", err)
	}
	log.Info("Relay pubkey: ", base64.URLEncoding.EncodeToString(relayKey.Public().MarshalRaw()))

	r := router.New(relayKey)
	http.Handle("/v1/relay", websocket.Config{Logger: log}.HTTPHandler(r))

	http.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("OK"))
	})

	log.Infof("Listening on %s...", *addr)
	err = http.ListenAndServe(*addr, nil)
	log.Info("Server exited: ", err)
}
