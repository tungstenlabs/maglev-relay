package auth

import (
	"maglev"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncodeDecodeNodeIDCredential(t *testing.T) {
	nodeID := maglev.NodeID("test")
	credential := EncodeNodeIDCredential(nodeID)
	decodedNodeID, err := DecodeNodeIDCredential(credential)
	assert.NoError(t, err)
	assert.Equal(t, nodeID, decodedNodeID)
}

func TestDecodeNodeIDCredentialInvalid(t *testing.T) {
	_, err := DecodeNodeIDCredential([]byte("invalid:nodeid"))
	assert.Error(t, err)
}
