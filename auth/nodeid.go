package auth

import (
	"crypto/subtle"
	"fmt"
	"maglev"
	"maglev/internal/cryptov1/keyderivation"
	"maglev/internal/cryptov1/signing"
	"maglev/internal/lowerbase32"
)

const nodeIDPublicKeyDeriveInfo = "WLABS-NODE-ID-V1"

func NodeIDFromPublicKey(pub *signing.PublicKey) (maglev.NodeID, error) {
	idBytes, err := nodeIDBytes(pub)
	if err != nil {
		return "", fmt.Errorf("derive key: %w", err)
	}
	return maglev.NodeID(lowerbase32.EncodeToString(idBytes)), nil
}

func ValidateNodeIDForPublicKey(nodeID maglev.NodeID, pub *signing.PublicKey) error {
	idBytes, err := lowerbase32.DecodeString(string(nodeID))
	if err != nil {
		return fmt.Errorf("decode: %w", err)
	}
	expectedBytes, err := nodeIDBytes(pub)
	if err != nil {
		return fmt.Errorf("derive key: %w", err)
	}
	if subtle.ConstantTimeCompare(idBytes, expectedBytes) == 0 {
		return fmt.Errorf("node ID doesn't match public key")
	}
	return nil
}

func nodeIDBytes(pub *signing.PublicKey) ([]byte, error) {
	pubBytes := pub.MarshalRaw()
	return keyderivation.DeriveKey(nodeIDPublicKeyDeriveInfo, pubBytes, 16)
}
