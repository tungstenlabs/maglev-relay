package auth

import (
	"maglev"
	"maglev/internal/cryptov1/testcrypto"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	testKey    = testcrypto.KeyA.Public()
	testNodeID = maglev.NodeID("qj7423qjuglrk6dl73w35xliyq")
)

func TestNodeIDFromPublicKey(t *testing.T) {
	nodeID, err := NodeIDFromPublicKey(testcrypto.KeyA.Public())
	assert.NoError(t, err)
	assert.Equal(t, testNodeID, nodeID)
}

func TestValidateNodeIDForPublicKey(t *testing.T) {
	err := ValidateNodeIDForPublicKey(testNodeID, testKey)
	assert.NoError(t, err)
}

func TestValidateNodeIDForPublicKeyBadNodeID(t *testing.T) {
	err := ValidateNodeIDForPublicKey(maglev.NodeID("not.base32"), testKey)
	assert.Error(t, err)
}

func TestValidateNodeIDForPublicKeyMismatch(t *testing.T) {
	err := ValidateNodeIDForPublicKey(testNodeID, testcrypto.KeyB.Public())
	assert.Error(t, err)
}
