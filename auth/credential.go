package auth

import (
	"bytes"
	"fmt"
	"maglev"
)

const nodeIDCredentialPrefix = "nodeid:"

func EncodeNodeIDCredential(nodeID maglev.NodeID) []byte {
	return []byte(nodeIDCredentialPrefix + nodeID)
}

func DecodeNodeIDCredential(credential []byte) (maglev.NodeID, error) {
	nodeIDBytes := bytes.TrimPrefix(credential, []byte(nodeIDCredentialPrefix))
	if len(nodeIDBytes) == len(credential) {
		return "", fmt.Errorf("invalid nodeid credential")
	}
	return maglev.NodeID(nodeIDBytes), nil
}
