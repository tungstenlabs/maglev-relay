package router

import (
	"context"
	"fmt"
	"maglev"
	"sync"

	"maglev/auth"
	"maglev/internal/cryptov1/signing"
	"maglev/statuserr"
	"maglev/transport"
	"maglev/transport/handshake"

	"go.uber.org/zap"
)

var contextKeyTestWaitGroup = struct{}{}

type Router struct {
	key *signing.PrivateKey

	nodeTransports    map[maglev.NodeID]*nodeTransport
	nodeTransportsMut sync.RWMutex
}

func New(key *signing.PrivateKey) *Router {
	return &Router{
		key: key,

		nodeTransports: make(map[maglev.NodeID]*nodeTransport),
	}
}

var _ transport.Server = (*Router)(nil)

func (r *Router) ServeTransport(ctx context.Context, t transport.Transport) error {
	log := t.Logger()

	// Perform handshake
	hsRes, err := handshake.Config{PrivateKey: r.key}.Perform(t)
	if err != nil {
		return fmt.Errorf("handshake: %w", err)
	}
	log.Debugf("Handshake complete: %#v", hsRes)

	// TODO: make auth pluggable
	if len(hsRes.Credential) > 0 {
		return fmt.Errorf("relay doesn't currently support authentication")
	}
	nodeID, err := auth.NodeIDFromPublicKey(hsRes.PublicKey)
	if err != nil {
		return fmt.Errorf("nodeID: %w", err)
	}

	r.addTransport(nodeID, t)
	defer r.removeTransport(nodeID, t)

	// Test helper
	if wg, ok := ctx.Value(contextKeyTestWaitGroup).(*sync.WaitGroup); ok {
		wg.Done()
	}

	log.Infof("Successful connection from %q", nodeID)

	for {
		msg := &transport.Msg{}
		if err := t.ReadMsg(msg); err != nil {
			if err == transport.ErrClosed {
				return nil
			}
			return err
		}
		if msg.SrcNodeId != "" && msg.SrcNodeId != string(nodeID) {
			return fmt.Errorf("bad src_node_id %q", msg.SrcNodeId)
		}
		msg.SrcNodeId = string(nodeID)
		go r.dispatch(msg, log, false)
	}
}

func (r *Router) dispatch(msg *transport.Msg, log *zap.SugaredLogger, recursive bool) {
	if msg.DestNodeId == "" {
		err := statuserr.CodeInvalidArgument.StatusError("destination missing")
		r.dispatchError(msg, err, log, recursive)
		return
	}

	nodeID := maglev.NodeID(msg.DestNodeId)
	t := r.getTransport(nodeID)
	if t == nil {
		log.Debugf("No transport for %+v", nodeID)
		err := statuserr.CodeNotFound.StatusError("destination not found")
		r.dispatchError(msg, err, log, recursive)
		return
	}

	t.Lock()
	defer t.Unlock()
	log.Debugf("Writing to %T %+v", t.Transport, nodeID)
	if err := t.WriteMsg(msg); err != nil {
		r.dispatchError(msg, err, log, recursive)
	}
}

func (r *Router) dispatchError(cause *transport.Msg, err error, log *zap.SugaredLogger, recursive bool) {
	msg := &transport.Msg{}
	msg.SrcNodeId = cause.DestNodeId
	msg.DestNodeId = cause.SrcNodeId
	msg.EncodeError(err)
	if recursive {
		log.With("cause", cause, "recursiveMsg", msg).Warn("Sending error failed")
		return
	}
	log.Debugf("Dispatching error %+v", msg)
	r.dispatch(msg, log, true)
}

func (r *Router) getTransport(nodeID maglev.NodeID) *nodeTransport {
	r.nodeTransportsMut.RLock()
	defer r.nodeTransportsMut.RUnlock()
	return r.nodeTransports[nodeID]
}

func (r *Router) addTransport(nodeID maglev.NodeID, t transport.Transport) {
	r.nodeTransportsMut.Lock()
	defer r.nodeTransportsMut.Unlock()

	transport, exists := r.nodeTransports[nodeID]
	if exists {
		transport.Lock()
		defer transport.Unlock()
		oldTransport := transport.Transport
		transport.Transport = t
		go oldTransport.Close(fmt.Errorf("replaced by new transport"))
	} else {
		r.nodeTransports[nodeID] = &nodeTransport{Transport: t}
	}
}

func (r *Router) removeTransport(nodeID maglev.NodeID, t transport.Transport) {
	r.nodeTransportsMut.Lock()
	defer r.nodeTransportsMut.Unlock()

	if r.nodeTransports[nodeID] == t {
		delete(r.nodeTransports, nodeID)
	}
}

type nodeTransport struct {
	sync.Mutex
	transport.Transport
}
