package router

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"maglev/auth"
	"maglev/internal/cryptov1/testcrypto"
	"maglev/statuserr"
	"maglev/transport"
	"maglev/transport/handshake"
	"maglev/transport/testtransport"
)

var (
	routerKey = testcrypto.KeyC
)

func TestRouter(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	router := New(routerKey)

	var routerWG, registrationWG sync.WaitGroup
	ctx = context.WithValue(ctx, contextKeyTestWaitGroup, &registrationWG)

	t.Log("Starting Node A...")
	clientA, routerA := testtransport.NewPair(t, ctx)
	routerWG.Add(1)
	go func() {
		defer routerWG.Done()
		err := router.ServeTransport(ctx, routerA)
		assert.NoError(t, err)
	}()
	registrationWG.Add(1)
	nodeA, err := auth.NodeIDFromPublicKey(testcrypto.KeyA.Public())
	require.NoError(t, err)
	_, err = handshake.Config{
		PrivateKey: testcrypto.KeyA,
	}.Perform(clientA)
	require.NoError(t, err)

	t.Log("Starting Node B...")
	clientB, routerB := testtransport.NewPair(t, ctx)
	routerWG.Add(1)
	go func() {
		defer routerWG.Done()
		err := router.ServeTransport(ctx, routerB)
		assert.NoError(t, err)
	}()
	registrationWG.Add(1)
	nodeB, err := auth.NodeIDFromPublicKey(testcrypto.KeyB.Public())
	require.NoError(t, err)
	_, err = handshake.Config{
		PrivateKey: testcrypto.KeyB,
	}.Perform(clientB)
	require.NoError(t, err)

	// Wait for both nodes to be registered by the Router...
	t.Log("Waiting for nodes to start...")
	registrationWG.Wait()

	t.Log("Sending from A to B...")
	msg := &transport.Msg{}
	msg.DestNodeId = string(nodeB)
	err = clientA.WriteMsg(msg)
	require.NoError(t, err)

	t.Log("Receiving on B...")
	msg.Reset()
	err = clientB.ReadMsg(msg)
	require.NoError(t, err)
	assert.Equal(t, string(nodeA), msg.SrcNodeId)

	t.Log("Sending from B to C (not found)")
	msg.Reset()
	msg.DestNodeId = "nodeC"
	err = clientB.WriteMsg(msg)
	require.NoError(t, err)
	err = clientB.ReadMsg(msg)
	require.NoError(t, err)
	assert.EqualValues(t, statuserr.CodeNotFound, msg.Error.Code)

	clientA.Close(nil)
	clientB.Close(nil)
	routerWG.Wait()
}
