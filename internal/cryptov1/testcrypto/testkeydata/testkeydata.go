package testkeydata

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"encoding/base64"
	"math/big"
)

var (
	KeyA = mustUnmarshalKey("AvVJR1VE9JvgTkWaNCc0VVWeybT0mVWYKad+y1O06I1P", "rDUR1hTgN747/3BN66UeQ2BTpBdpvqSJTdzhXfahe2s")
	KeyB = mustUnmarshalKey("A/FDl1tKhIxo+a7FbBl8XxpaE+8sqZ3PkwW4bMpf0rGo", "Utmr3YGyu2ZH+WPtk8iiXnp3T9RTRMjbxvG//szIvmQ")
	KeyC = mustUnmarshalKey("AieO5HrsIacIwfCCNHkU+fZPg8NvcpfXcFbn4aURNIVu", "egBXdBAYZeU3xKrK/zujxqlcmxLOc1Y081m7nJ0y044")
)

func mustUnmarshalKey(pubStr, privStr string) *ecdsa.PrivateKey {
	key := &ecdsa.PrivateKey{}
	key.Curve = elliptic.P256()
	key.X, key.Y = elliptic.UnmarshalCompressed(key.Curve, mustDecode(pubStr))
	if !key.IsOnCurve(key.X, key.Y) {
		panic("pubkey not on curve")
	}
	key.D = &big.Int{}
	key.D.SetBytes(mustDecode(privStr))
	return key
}

func mustDecode(s string) []byte {
	b, err := base64.RawStdEncoding.DecodeString(s)
	if err != nil {
		panic("decode base64")
	}
	return b
}
