package testcrypto

import (
	"maglev/internal/cryptov1/signing"
	"maglev/internal/cryptov1/testcrypto/testkeydata"
)

var (
	KeyA = &signing.PrivateKey{PrivateKey: testkeydata.KeyA}
	KeyB = &signing.PrivateKey{PrivateKey: testkeydata.KeyB}
	KeyC = &signing.PrivateKey{PrivateKey: testkeydata.KeyC}
)
