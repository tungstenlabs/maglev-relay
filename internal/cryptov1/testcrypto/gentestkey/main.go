package main

import (
	"crypto/elliptic"
	"encoding/base64"
	"fmt"
	"maglev/internal/cryptov1/signing"
)

func main() {
	key, err := signing.GenerateKey()
	if err != nil {
		panic(err)
	}
	rawPub := elliptic.MarshalCompressed(key, key.X, key.Y)
	pub64 := base64.RawStdEncoding.EncodeToString(rawPub)
	priv64 := base64.RawStdEncoding.EncodeToString(key.D.Bytes())

	fmt.Printf(`KeyXXX = mustUnmarshalKey(%q, %q)`, pub64, priv64)
}
