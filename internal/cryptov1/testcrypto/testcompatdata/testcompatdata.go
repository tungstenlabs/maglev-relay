package testcompatdata

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"math/big"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

const testDataGlob = "../../../../testdata/cryptov1/*.json"

var (
	TestSignaturePrefix = "testPrefix"
	TestSignatureParts  = [][]byte{
		[]byte("sigPart1"),
		[]byte("sigPartTwo"),
	}

	TestKeyDerivationSecret = []byte("testSecret")
	TestKeyDerivationInfo   = "testInfo"

	TestKeyAgreementOutputLength = 16
)

type TestData struct {
	SigningKey       *ecdsa.PrivateKey
	SigningSignature []byte

	KeyDerivationOutput []byte

	KeyAgreementKey    *ecdsa.PrivateKey // Not technically ECDSA...
	KeyAgreementOutput []byte
}

type testDataJSON struct {
	SigningKeyJWK    ecJWK  `json:"signingKeyJWK"`
	SigningSignature []byte `json:"signingSignature"`

	KeyDerivationOutput []byte `json:"keyderivationOutput"`

	KeyAgreementKeyJWK ecJWK  `json:"keyagreementKeyJWK"`
	KeyAgreementOutput []byte `json:"keyagreementOutput"`
}

type ecJWK struct {
	D string `json:"d"`
	X string `json:"x"`
	Y string `json:"y"`
}

func (k ecJWK) decodePrivateKey(t *testing.T) *ecdsa.PrivateKey {
	return &ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{
			Curve: elliptic.P256(),
			X:     bigIntFromJWK(t, k.X),
			Y:     bigIntFromJWK(t, k.Y),
		},
		D: bigIntFromJWK(t, k.D),
	}
}

var testDataFiles map[string]*TestData

func GetTestData(t *testing.T) map[string]*TestData {
	if testDataFiles != nil {
		return testDataFiles
	}
	matches, err := filepath.Glob(testDataGlob)
	if err != nil {
		t.Fatal(err)
	}
	if len(matches) == 0 {
		cwd, _ := os.Getwd()
		t.Log("CWD: ", cwd)
		t.Skipf("no testdata found at %q", testDataGlob)
	}
	testDataFiles = make(map[string]*TestData)
	for _, filename := range matches {
		data, err := ioutil.ReadFile(filename)
		if err != nil {
			t.Fatalf("Reading %s: %v", filename, err)
		}
		filename = strings.TrimLeft(filename, "/.")
		var dataJSON testDataJSON
		if err := json.Unmarshal(data, &dataJSON); err != nil {
			t.Fatalf("Unmarshaling %s: %v", filename, err)
		}
		testData := &TestData{
			SigningKey:       dataJSON.SigningKeyJWK.decodePrivateKey(t),
			SigningSignature: dataJSON.SigningSignature,

			KeyDerivationOutput: dataJSON.KeyDerivationOutput,

			KeyAgreementKey:    dataJSON.KeyAgreementKeyJWK.decodePrivateKey(t),
			KeyAgreementOutput: dataJSON.KeyAgreementOutput,
		}
		if !testData.SigningKey.IsOnCurve(testData.SigningKey.X, testData.SigningKey.Y) {
			t.Fatalf("invalid key in %s", filename)
		}
		testDataFiles[filename] = testData
	}
	return testDataFiles
}

func bigIntFromJWK(t *testing.T, b64 string) *big.Int {
	bs, err := base64.RawURLEncoding.DecodeString(b64)
	if err != nil {
		t.Fatalf("decoding %q: %v", b64, err)
	}
	i := &big.Int{}
	i.SetBytes(bs)
	return i
}
