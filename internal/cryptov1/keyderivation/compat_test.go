package keyderivation

import (
	"testing"

	"maglev/internal/cryptov1/testcrypto/testcompatdata"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCompatDeriveKey(t *testing.T) {
	for filename, data := range testcompatdata.GetTestData(t) {
		output, err := DeriveKey(testcompatdata.TestKeyDerivationInfo, testcompatdata.TestKeyDerivationSecret, 4)
		require.NoError(t, err)

		assert.Equalf(t, data.KeyDerivationOutput, output, "output mismatch from %q", filename)
	}
}
