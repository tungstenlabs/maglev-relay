// Key derivation, compatible with Web Crypto's HKDF/SHA-256
package keyderivation

import (
	"crypto/sha256"

	"golang.org/x/crypto/hkdf"
)

func DeriveKey(info string, secret []byte, len int) ([]byte, error) {
	r := hkdf.New(sha256.New, secret, []byte{}, []byte(info))
	out := make([]byte, len)
	if _, err := r.Read(out); err != nil {
		return nil, err
	}
	return out, nil
}
