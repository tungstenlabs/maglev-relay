// Public key agreement, compatible with Web Crypto's ECDH/P-256 operations.
package keyagreement

import (
	"crypto/elliptic"
	"crypto/rand"
	"errors"
	"math/big"
)

var (
	Curve = elliptic.P256()

	ErrInvalidPublicKey = errors.New("invalid public key")
)

type PrivateKey struct {
	PublicKey
	priv []byte
}

func GenerateKey() (*PrivateKey, error) {
	priv, x, y, err := elliptic.GenerateKey(Curve, rand.Reader)
	if err != nil {
		return nil, err
	}
	return &PrivateKey{
		PublicKey: PublicKey{X: x, Y: y},
		priv:      priv,
	}, nil
}

func (k *PrivateKey) DeriveSharedKey(pub *PublicKey) []byte {
	x, _ := Curve.ScalarMult(pub.X, pub.Y, k.priv)
	return x.Bytes()
}

type PublicKey struct {
	X, Y *big.Int
}

func UnmarshalRawPublicKey(raw []byte) (*PublicKey, error) {
	x, y := elliptic.Unmarshal(Curve, raw)
	if x == nil {
		return nil, ErrInvalidPublicKey
	}
	return &PublicKey{X: x, Y: y}, nil
}

func (k *PublicKey) MarshalRaw() []byte {
	return elliptic.Marshal(Curve, k.X, k.Y)
}
