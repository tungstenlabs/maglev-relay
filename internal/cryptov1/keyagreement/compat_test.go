package keyagreement

import (
	"testing"

	"maglev/internal/cryptov1/testcrypto/testcompatdata"

	"github.com/stretchr/testify/assert"
)

func TestCompatDeriveKey(t *testing.T) {
	for filename, data := range testcompatdata.GetTestData(t) {
		keyData := data.KeyAgreementKey
		key := &PrivateKey{
			PublicKey: PublicKey{
				X: keyData.X,
				Y: keyData.Y,
			},
			priv: keyData.D.Bytes(),
		}
		output := key.DeriveSharedKey(&key.PublicKey)[:testcompatdata.TestKeyAgreementOutputLength]

		assert.Equalf(t, data.KeyAgreementOutput, output, "output mismatch from %q", filename)
	}
}
