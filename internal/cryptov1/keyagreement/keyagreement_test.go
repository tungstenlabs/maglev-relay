// Public key agreement, compatible with Web Crypto's ECDH/P-256 operations.
package keyagreement

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDeriveKey(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}

	k1, err := GenerateKey()
	require.NoError(t, err)
	k2, err := GenerateKey()
	require.NoError(t, err)

	assert.Equal(t, k1.DeriveSharedKey(&k2.PublicKey), k2.DeriveSharedKey(&k1.PublicKey))
}
