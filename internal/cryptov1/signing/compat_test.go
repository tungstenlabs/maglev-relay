package signing

import (
	"testing"

	"maglev/internal/cryptov1/testcrypto/testcompatdata"

	"github.com/stretchr/testify/assert"
)

func TestCompatVerify(t *testing.T) {
	testLogging(t)
	for filename, data := range testcompatdata.GetTestData(t) {
		t.Logf("Key: %#v", data.SigningKey)
		key := &PrivateKey{data.SigningKey}
		assert.Truef(t, key.Public().VerifyParts(
			data.SigningSignature,
			testcompatdata.TestSignaturePrefix,
			testcompatdata.TestSignatureParts...),
			"signature verification failed from %q", filename)
	}
}
