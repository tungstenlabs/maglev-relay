package signing

import (
	"encoding/base64"
	"maglev/internal/cryptov1/testcrypto/testkeydata"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
)

var (
	testKey       = &PrivateKey{testkeydata.KeyA}
	testPublicKey = testKey.Public()

	testPrefix          = "testPrefix"
	testPayload         = []byte{1, 2, 3, 4}
	testSignature, _    = base64.StdEncoding.DecodeString("QT9WXQG9x619Uxa0db0wT1mJ0ye0LZGcJDLcyeVLhcx0k/sl+TkYVrpIypHuWDs6QgR9laGZvdbqoVT8dbBv0g==")
	testPublicKeyRaw, _ = base64.StdEncoding.DecodeString("BPVJR1VE9JvgTkWaNCc0VVWeybT0mVWYKad+y1O06I1P4tE9ux11q4iWNMp4geDrj9mLUYa83Wf7lYsmbMWOuwY=")

	testOtherKey = &PrivateKey{testkeydata.KeyB}
)

func TestGeneratePrivateKey(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}
	k, err := GenerateKey()
	require.NoError(t, err)

	assert.True(t, Curve.IsOnCurve(k.X, k.Y))
}

func TestPublicKeyMarshalRaw(t *testing.T) {
	raw := testPublicKey.MarshalRaw()
	assert.Equal(t, testPublicKeyRaw, raw)
}

func TestVerify(t *testing.T) {
	testLogging(t)

	assert.True(t, testPublicKey.VerifyParts(testSignature, testPrefix, testPayload))

	// Wrong public key
	assert.False(t, testOtherKey.Public().VerifyParts(testSignature, testPrefix, testPayload))
	// Wrong signature
	assert.False(t, testPublicKey.VerifyParts(testSignature[1:], testPrefix, testPayload))
	// Wrong prefix
	assert.False(t, testPublicKey.VerifyParts(testSignature, "badPrefix", testPayload))
	// Wrong payload (split)
	assert.False(t, testPublicKey.VerifyParts(testSignature, testPrefix, testPayload[:2], testPayload[2:]))
	// Wrong payload (extra suffix)
	assert.False(t, testPublicKey.VerifyParts(testSignature, testPrefix, testPayload, []byte("bad suffix")))
}

func TestSignVerify(t *testing.T) {
	sig, err := testKey.SignParts(testPrefix, testPayload)
	require.NoError(t, err)
	t.Log("Sig: ", base64.StdEncoding.EncodeToString(sig))

	assert.True(t, testPublicKey.VerifyParts(sig, testPrefix, testPayload))
}

func testLogging(t *testing.T) {
	zap.ReplaceGlobals(zaptest.NewLogger(t))
}
