// Public key signing, compatible with Web Crypto's ECDSA/P-256/SHA-256 operations.
package signing

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/big"

	"go.uber.org/zap"
)

var (
	Curve = elliptic.P256()

	elementSize   = Curve.Params().BitSize / 8
	SignatureSize = elementSize * 2

	ErrEmptyPayload     = errors.New("empty payload")
	ErrInvalidPublicKey = errors.New("invalid public key")
)

type PrivateKey struct {
	*ecdsa.PrivateKey
}

// GenerateKey returns a random PrivateKey.
func GenerateKey() (*PrivateKey, error) {
	priv, err := ecdsa.GenerateKey(Curve, rand.Reader)
	if err != nil {
		return nil, err
	}
	return &PrivateKey{priv}, nil
}

func (k *PrivateKey) SignParts(prefix string, payloadParts ...[]byte) ([]byte, error) {
	sum, err := hashSum(prefix, payloadParts)
	if err != nil {
		return nil, err
	}
	r, s, err := ecdsa.Sign(rand.Reader, k.PrivateKey, sum)
	if err != nil {
		return nil, err
	}
	buf := make([]byte, elementSize*2)
	r.FillBytes(buf[:elementSize])
	s.FillBytes(buf[elementSize:])
	return buf, nil
}

func (k *PrivateKey) Public() *PublicKey {
	return &PublicKey{&k.PublicKey}
}

type PublicKey struct {
	*ecdsa.PublicKey
}

// UnmarshalRawPublicKey decodes a PublicKey from a form that can be encoded
// by Web Crypto's exportKey function.
func UnmarshalRawPublicKey(raw []byte) (*PublicKey, error) {
	x, y := elliptic.Unmarshal(Curve, raw)
	if x == nil {
		return nil, ErrInvalidPublicKey
	}
	key := &ecdsa.PublicKey{Curve: Curve, X: x, Y: y}
	return &PublicKey{key}, nil
}

func (k *PublicKey) MarshalRaw() []byte {
	return elliptic.Marshal(Curve, k.X, k.Y)
}

func (k *PublicKey) VerifyParts(signature []byte, prefix string, payloadParts ...[]byte) bool {
	if len(signature) != SignatureSize {
		zap.S().Debug("VerifyParts invalid signature size ", len(signature))
		return false
	}
	sum, err := hashSum(prefix, payloadParts)
	if err != nil {
		zap.S().Debug("VerifyParts hashSum error: ", err)
		return false
	}
	r := &big.Int{}
	s := &big.Int{}
	r.SetBytes(signature[:elementSize])
	s.SetBytes(signature[elementSize:])
	return ecdsa.Verify(k.PublicKey, sum, r, s)
}

func (k *PublicKey) Equal(other *PublicKey) bool {
	return k.PublicKey.Equal(other.PublicKey)
}

var _ json.Marshaler = (*PublicKey)(nil)

func (k *PublicKey) MarshalJSON() ([]byte, error) {
	return json.Marshal(k.MarshalRaw())
}

func hashSum(prefix string, parts [][]byte) ([]byte, error) {
	if len(parts) == 0 || len(parts[0]) == 0 {
		return nil, ErrEmptyPayload
	}
	parts = append([][]byte{[]byte(prefix)}, parts...)
	h := sha256.New()
	var w io.Writer = h
	// Uncomment for debugging:
	//w = iotest.NewWriteLogger("hashSum: ", w)
	for _, part := range parts {
		if len(part) > 0xffff {
			return nil, fmt.Errorf("part length %d > max 65535", len(part))
		}
		binary.Write(w, binary.BigEndian, uint16(len(part)))
		w.Write(part)
	}
	return h.Sum(nil), nil
}
