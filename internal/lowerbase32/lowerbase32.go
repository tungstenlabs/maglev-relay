package lowerbase32

import (
	"crypto/rand"
	"encoding/base32"
)

var Encoding = base32.NewEncoding("abcdefghijklmnopqrstuvwxyz234567").WithPadding(base32.NoPadding)

func EncodeToString(src []byte) string {
	return Encoding.EncodeToString(src)
}

func DecodeString(s string) ([]byte, error) {
	return Encoding.DecodeString(s)
}

func RandomIdent(entropyBytes int) string {
	randBytes := make([]byte, entropyBytes)
	rand.Read(randBytes)
	return EncodeToString(randBytes)
}
