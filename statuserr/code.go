package statuserr

import (
	"errors"
	"fmt"
)

type Code int32

const (
	// These are from google.rpc.Code
	CodeOK                 Code = 0
	CodeCancelled          Code = 1
	CodeUnknown            Code = 2
	CodeInvalidArgument    Code = 3
	CodeDeadlineExceeded   Code = 4
	CodeNotFound           Code = 5
	CodeAlreadyExists      Code = 6
	CodePermissionDenied   Code = 7
	CodeResourceExhausted  Code = 8
	CodeFailedPrecondition Code = 9
	CodeAborted            Code = 10
	CodeOutOfRange         Code = 11
	CodeUnimplemented      Code = 12
	CodeInternal           Code = 13
	CodeUnavailable        Code = 14
	CodeDataLoss           Code = 15
	CodeUnauthenticated    Code = 16
)

func CodeFor(err error) Code {
	if err == nil {
		return CodeOK
	}
	if statusErr := (StatusError{}); errors.As(err, &statusErr) {
		return statusErr.Code
	}
	return CodeUnknown
}

func (c Code) Error() string {
	return c.DefaultMessage()
}

func (c Code) StatusError(a ...interface{}) StatusError {
	se := StatusError{Code: c}
	if len(a) == 0 || a[0] == nil || a[0] == "" {
		se.Message = c.DefaultMessage()
	} else {
		se.Message = fmt.Sprint(a...)
	}
	return se
}

func (c Code) StatusErrorf(format string, a ...interface{}) StatusError {
	return StatusError{
		Code:    c,
		Message: fmt.Sprintf(format, a...),
	}
}

func (c Code) Wrap(err error) StatusError {
	return StatusError{
		Code:    c,
		Message: err.Error(),
		cause:   err,
	}
}

func (c Code) DefaultMessage() string {
	switch c {
	case CodeOK:
		return "ok"
	case CodeCancelled:
		return "cancelled"
	case CodeUnknown:
		return "unknown"
	case CodeInvalidArgument:
		return "invalid argument"
	case CodeDeadlineExceeded:
		return "deadline exceeded"
	case CodeNotFound:
		return "not found"
	case CodeAlreadyExists:
		return "already exists"
	case CodePermissionDenied:
		return "permission denied"
	case CodeResourceExhausted:
		return "resource exhausted"
	case CodeFailedPrecondition:
		return "failed precondition"
	case CodeAborted:
		return "aborted"
	case CodeOutOfRange:
		return "out of range"
	case CodeUnimplemented:
		return "unimplemented"
	case CodeInternal:
		return "internal"
	case CodeUnavailable:
		return "unavailable"
	case CodeDataLoss:
		return "data loss"
	case CodeUnauthenticated:
		return "unauthenticated"
	}
	return ""
}
