package statuserr

import (
	"fmt"

	"maglev/protogen/transport"
)

type StatusError struct {
	Code    Code
	Message string
	cause   error
}

func For(err error) StatusError {
	return CodeFor(err).Wrap(err)
}

func FromProto(status *transport.Status) error {
	if status == nil || status.Code == 0 {
		return nil
	}
	return Code(status.Code).StatusError(status.Message)
}

func (se StatusError) ToProto() *transport.Status {
	return &transport.Status{
		Code:    int32(se.Code),
		Message: se.Message,
	}
}

func (se StatusError) Error() string {
	return fmt.Sprintf("status %d: %+q", se.Code, se.Message)
}

func (se StatusError) Unwrap() error {
	return se.cause
}
