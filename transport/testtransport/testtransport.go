package testtransport

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
	"google.golang.org/protobuf/proto"

	"maglev/internal/lowerbase32"
	"maglev/transport"
)

type TestTransport struct {
	T                   *testing.T
	id                  string
	ctx                 context.Context
	readChan, writeChan chan *transport.Msg
}

func New(t *testing.T, ctx context.Context) *TestTransport {
	return &TestTransport{
		T:   t,
		ctx: ctx,

		id:        lowerbase32.RandomIdent(4),
		readChan:  make(chan *transport.Msg),
		writeChan: make(chan *transport.Msg),
	}
}

func (t *TestTransport) TestRecv() *transport.Msg {
	select {
	case msg := <-t.writeChan:
		return msg
	case <-t.ctx.Done():
		t.T.Fatal(t.ctx.Err())
		return nil
	}
}

func (t *TestTransport) TestSend(msg *transport.Msg) {
	if msg == nil {
		close(t.readChan)
		return
	}
	msg = msg.Clone()
	select {
	case t.readChan <- msg:
	case <-t.ctx.Done():
		t.T.Fatal(t.ctx.Err())
	}
}

var _ transport.Transport = (*TestTransport)(nil)

func (TestTransport) Type() transport.Type {
	return transport.Type("test")
}

func (t *TestTransport) Logger() *zap.SugaredLogger {
	return zaptest.NewLogger(t.T).Sugar().With("id", t.id)
}

func (t *TestTransport) ReadMsg(msg *transport.Msg) error {
	select {
	case sent, ok := <-t.readChan:
		if !ok {
			t.T.Logf("[%s] ReadMsg: closed", t.id)
			return transport.ErrClosed
		}
		t.T.Logf("[%s] ReadMsg: %+v", t.id, sent)
		proto.Merge(&msg.MsgHeader, &sent.MsgHeader)
		msg.Payload = sent.Payload
		return nil
	case <-t.ctx.Done():
		return t.ctx.Err()
	}
}

func (t *TestTransport) WriteMsg(msg *transport.Msg) error {
	msg = msg.Clone()
	t.T.Logf("[%s] WriteMsg: %+v", t.id, msg)
	select {
	case t.writeChan <- msg:
		return nil
	case <-t.ctx.Done():
		return t.ctx.Err()
	}
}

func (t *TestTransport) Close(err error) {
	t.T.Logf("[%s] Closing: %v", t.id, err)
	close(t.readChan)
	close(t.writeChan)
	require.NoError(t.T, err)
}

func NewPair(t *testing.T, ctx context.Context) (transport.Transport, transport.Transport) {
	pairID := lowerbase32.RandomIdent(4)
	chanA := make(chan *transport.Msg, 1)
	chanB := make(chan *transport.Msg, 1)
	t1 := &TestTransport{T: t, ctx: ctx, id: pairID + ".A", readChan: chanA, writeChan: chanB}
	t2 := &TestTransport{T: t, ctx: ctx, id: pairID + ".B", readChan: chanB, writeChan: chanA}
	return t1, t2
}
