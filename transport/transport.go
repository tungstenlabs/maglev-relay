package transport

import (
	"context"
	"errors"
	"fmt"

	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

var (
	// ErrClosed may be returned by a Transport to indicate that it was closed normally.
	ErrClosed = errors.New("closed")
)

type ExchangeID int64

type Type string

type Transport interface {
	Type() Type
	Logger() *zap.SugaredLogger
	ReadMsg(*Msg) error
	WriteMsg(*Msg) error
	Close(error)
}

type Server interface {
	ServeTransport(context.Context, Transport) error
}

func ReadProtoPayloadMsg(t Transport, msg *Msg, payload proto.Message) error {
	if err := t.ReadMsg(msg); err != nil {
		return fmt.Errorf("read %T: %w", payload, err)
	}
	if err := msg.DecodeProtoPayload(payload); err != nil {
		return err
	}
	t.Logger().Debugf("Received %T: %s", payload, payload)
	return nil
}

func WriteProtoPayloadMsg(t Transport, msg *Msg, payload proto.Message) error {
	if err := msg.EncodeProtoPayload(payload); err != nil {
		return fmt.Errorf("encode %T: %w", payload, err)
	}
	t.Logger().Debugf("Sending %T: %s", payload, payload)
	if err := t.WriteMsg(msg); err != nil {
		return fmt.Errorf("write %T: %w", payload, err)
	}
	return nil
}
