package websocket

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"sync/atomic"
	"time"

	"github.com/gorilla/websocket"
	"go.uber.org/zap"

	"maglev/transport"
)

const TransportType = transport.Type("ws")

var (
	// Websocket transports will send Pings this often if no other traffic is received
	pingInterval = 30 * time.Second
	// Websocket Ping/Pong messages will timeout after this long
	pingTimeout = 10 * time.Second
	// Websocket close messages will timeout after this long (and close the connection)
	closeTimeout = 1 * time.Second

	upgrader = websocket.Upgrader{
		// TODO: Make origin check configurable
		CheckOrigin: func(r *http.Request) bool { return true },
	}
)

type Config struct {
	Logger *zap.SugaredLogger
}

func (c Config) logger() *zap.SugaredLogger {
	logger := c.Logger
	if logger == nil {
		logger = zap.S()
	}
	return logger.Named("websocket")
}

func (c Config) startTransport(ctx context.Context, conn *websocket.Conn) *wsTransport {
	log := c.logger().With("remoteAddr", conn.RemoteAddr().String())
	t := &wsTransport{conn: conn, log: log}
	t.start(ctx)
	return t
}

func (c Config) Dial(ctx context.Context, url string) (*wsTransport, error) {
	conn, _, err := websocket.DefaultDialer.DialContext(ctx, url, nil)
	if err != nil {
		return nil, fmt.Errorf("dial: %w", err)
	}
	return c.startTransport(ctx, conn), nil
}

func (c Config) Accept(w http.ResponseWriter, r *http.Request) (*wsTransport, error) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return nil, fmt.Errorf("upgrade: %w", err)
	}
	return c.startTransport(r.Context(), conn), nil
}

func (c Config) HTTPHandler(server transport.Server) *wsHandler {
	return &wsHandler{
		config: c,
		server: server,
	}
}

type wsHandler struct {
	config Config
	server transport.Server
}

var _ http.Handler = (*wsHandler)(nil)

func (h *wsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	t, err := h.config.Accept(w, r)
	if err != nil {
		h.config.logger().With(zap.Error(err)).Error("Accept failed")
		return
	}
	err = h.server.ServeTransport(r.Context(), t)
	if err != nil {
		// Make unexpected EOF errors quieter
		closeErr := &websocket.CloseError{}
		if errors.As(err, &closeErr) && closeErr.Text == io.ErrUnexpectedEOF.Error() {
			t.Logger().Info("Connection failed: ", err)
		} else {
			t.Logger().With(zap.Error(err)).Warn("Connection failed")
		}
	} else {
		t.Logger().Info("Connection closed normally")
	}
	t.Close(err)
}

type wsTransport struct {
	conn *websocket.Conn
	log  *zap.SugaredLogger

	lastMsgTime atomic.Value
}

func (t *wsTransport) start(ctx context.Context) {
	t.conn.SetPingHandler(t.pingHandler)
	t.conn.SetPongHandler(t.pongHandler)
	go t.pingLoop(ctx)
}

func (t *wsTransport) updateLastMsgTime() {
	t.lastMsgTime.Store(time.Now())
}

func (t *wsTransport) pingHandler(data string) error {
	t.log.Debugf("Got ping: %+q", data)
	t.updateLastMsgTime()
	defer t.updateLastMsgTime()
	return t.conn.WriteControl(websocket.PongMessage, []byte(data), time.Now().Add(pingTimeout))
}

func (t *wsTransport) pongHandler(data string) error {
	t.log.Debugf("Got pong: %+q", data)
	t.updateLastMsgTime()
	return nil
}

func (t *wsTransport) pingLoop(ctx context.Context) {
	t.lastMsgTime.Store(time.Now())
	for ctx.Err() == nil {
		pingDeadline := t.lastMsgTime.Load().(time.Time).Add(pingInterval)
		if time.Now().After(pingDeadline) {
			t.log.Debug("Sending ping...")
			err := t.conn.WriteControl(websocket.PongMessage, []byte{}, time.Now().Add(pingTimeout))
			if err != nil {
				t.log.Debug("Ping failed: ", err)
				break
			}
			t.updateLastMsgTime()
		} else {
			time.Sleep(time.Until(pingDeadline))
		}
	}
}

var _ transport.Transport = (*wsTransport)(nil)

func (wsTransport) Type() transport.Type {
	return TransportType
}

func (t *wsTransport) Logger() *zap.SugaredLogger {
	return t.log
}

func (t *wsTransport) ReadMsg(msg *transport.Msg) error {
	defer t.updateLastMsgTime()
	_, rawMsg, err := t.conn.ReadMessage()
	if err != nil {
		if websocket.IsCloseError(err, websocket.CloseNormalClosure) {
			return transport.ErrClosed
		}
		return fmt.Errorf("read msg: %w", err)
	}
	if err := msg.UnmarshalBinary(rawMsg); err != nil {
		return fmt.Errorf("unmarshal msg: %w", err)
	}
	return nil
}

func (t *wsTransport) WriteMsg(msg *transport.Msg) error {
	defer t.updateLastMsgTime()
	rawMsg, err := msg.MarshalBinary()
	if err != nil {
		return fmt.Errorf("marshal msg: %w", err)
	}
	if err := t.conn.WriteMessage(websocket.BinaryMessage, rawMsg); err != nil {
		if err == websocket.ErrCloseSent {
			return transport.ErrClosed
		}
		return fmt.Errorf("write msg: %w", err)
	}
	return nil
}

func (t *wsTransport) Close(err error) {
	defer t.conn.Close()
	var msg []byte
	if err != nil {
		// TODO: probably don't send _all_ errors back
		msg = websocket.FormatCloseMessage(websocket.CloseAbnormalClosure, err.Error())
	} else {
		msg = websocket.FormatCloseMessage(websocket.CloseNormalClosure, "")
	}
	t.conn.WriteControl(websocket.CloseMessage, msg, time.Now().Add(closeTimeout))
}
