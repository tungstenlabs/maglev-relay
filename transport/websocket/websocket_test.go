package websocket

import (
	"context"
	"maglev/transport"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTransport(t *testing.T) {
	config := &Config{}

	ts := &testTransportServer{T: t}
	srv := httptest.NewServer(config.HTTPHandler(ts))
	defer srv.Close()
	url := strings.Replace(srv.URL, "http", "ws", 1)

	client, err := config.Dial(context.Background(), url)
	require.NoError(t, err)

	client.conn.WriteMessage(websocket.PingMessage, []byte{})

	msg := &transport.Msg{Payload: []byte("hello")}
	err = client.WriteMsg(msg)
	require.NoError(t, err)

	err = client.ReadMsg(msg)
	require.NoError(t, err)

	assert.Equal(t, []byte("hello"), ts.msg.Payload)
	assert.Equal(t, []byte("goodbye"), msg.Payload)
}

type testTransportServer struct {
	T   *testing.T
	msg transport.Msg
}

func (ts *testTransportServer) ServeTransport(_ context.Context, t transport.Transport) error {
	err := t.ReadMsg(&ts.msg)
	require.NoError(ts.T, err)

	msg := &transport.Msg{Payload: []byte("goodbye")}
	err = t.WriteMsg(msg)
	require.NoError(ts.T, err)
	return nil
}
