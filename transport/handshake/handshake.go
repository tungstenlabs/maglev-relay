package handshake

import (
	"encoding/binary"
	"errors"
	"fmt"

	"maglev/internal/cryptov1/keyagreement"
	"maglev/internal/cryptov1/keyderivation"
	"maglev/internal/cryptov1/signing"
	"maglev/protogen/handshake"
	"maglev/transport"
)

var (
	ErrBadSignature = errors.New("bad signature")

	helloVersion                 uint32 = 1
	helloResponseSignaturePrefix        = "WLABS-HELLO-RESPONSE-V1"

	sessionIDDeriveInfo = "WLABS-SESSION-ID-V1"
)

type Results struct {
	// Negotiated session ID
	SessionID uint64
	// True if session was just created
	NewSession bool
	// Peer public key
	PublicKey *signing.PublicKey
	// Peer credential (e.g. token)
	Credential []byte
}

type Config struct {
	// This node's private key
	PrivateKey *signing.PrivateKey
	// If set, attempt to resume this session
	SessionID uint64
	// If set, this credential will be sent to the peer
	Credential []byte
}

func (c Config) Perform(t transport.Transport) (*Results, error) {
	hs := New(c)
	hello, err := hs.InitiateHello()
	if err != nil {
		return nil, err
	}
	msg := &transport.Msg{}
	msg.ExchangeType = "Hello"
	if err := transport.WriteProtoPayloadMsg(t, msg, hello); err != nil {
		return nil, err
	}

	for {
		msg := &transport.Msg{}
		if err := t.ReadMsg(msg); err != nil {
			return nil, err
		}
		switch msg.ExchangeType {
		case "Hello":
			hello := &handshake.Hello{}
			if err := msg.DecodeProtoPayload(hello); err != nil {
				return nil, err
			}
			resp, err := hs.ProcessHello(hello)
			if err != nil {
				return nil, err
			}
			msg.Reset()
			msg.ExchangeType = "HelloResponse"
			if err := transport.WriteProtoPayloadMsg(t, msg, resp); err != nil {
				return nil, err
			}
		case "HelloResponse":
			resp := &handshake.HelloResponse{}
			if err := msg.DecodeProtoPayload(resp); err != nil {
				return nil, err
			}
			result, resp, err := hs.ProcessHelloResponse(resp)
			if err != nil {
				return nil, err
			}
			if resp != nil {
				msg.Reset()
				msg.ExchangeType = "HelloResponse"
				if err := transport.WriteProtoPayloadMsg(t, msg, resp); err != nil {
					return nil, err
				}
			}
			return result, nil
		default:
			return nil, fmt.Errorf("unexpected handshake exchange type %q", msg.ExchangeType)
		}
	}
}

type handshakeState string

const (
	stateInit         handshakeState = "INIT"
	stateSentHello    handshakeState = "SENT_HELLO"
	stateSentResponse handshakeState = "SENT_RESPONSE"
	stateDone         handshakeState = "DONE"
)

var (
	validTransitions = map[handshakeState]map[handshakeState]bool{
		stateInit:         {stateSentHello: true, stateSentResponse: true},
		stateSentHello:    {stateSentResponse: true, stateDone: true},
		stateSentResponse: {stateDone: true},
	}
)

type Handshake struct {
	config Config

	state     handshakeState
	ephKey    *keyagreement.PrivateKey
	sharedKey []byte
	sessionID uint64
}

func New(config Config) *Handshake {
	return &Handshake{config: config, state: stateInit}
}

func (h *Handshake) transition(state handshakeState) error {
	if !validTransitions[h.state][state] {
		return fmt.Errorf("invalid transition: %s -> %s", h.state, state)
	}
	h.state = state
	return nil
}

func (h *Handshake) InitiateHello() (*handshake.Hello, error) {
	if err := h.transition(stateSentHello); err != nil {
		return nil, err
	}
	ephKey, err := h.getEphemeralKey()
	if err != nil {
		return nil, err
	}
	return &handshake.Hello{
		Version:            helloVersion,
		ResumeSessionId:    h.config.SessionID,
		EphemeralPublicKey: ephKey.MarshalRaw(),
	}, nil
}

func (h *Handshake) ProcessHello(hello *handshake.Hello) (*handshake.HelloResponse, error) {
	if err := h.transition(stateSentResponse); err != nil {
		return nil, err
	}
	if hello.Version != helloVersion {
		return nil, fmt.Errorf("unsupported handshake version %d", hello.Version)
	}

	sharedKey, err := h.getSharedKey(hello.EphemeralPublicKey)
	if err != nil {
		return nil, err
	}
	sessionID, err := h.getSessionID(hello.ResumeSessionId, sharedKey)
	if err != nil {
		return nil, err
	}
	return h.buildResponse(sessionID, sharedKey, h.ephKey.MarshalRaw())
}

func (h *Handshake) ProcessHelloResponse(resp *handshake.HelloResponse) (*Results, *handshake.HelloResponse, error) {
	prevState := h.state
	if err := h.transition(stateDone); err != nil {
		return nil, nil, err
	}

	peerPublicKey, err := signing.UnmarshalRawPublicKey(resp.PublicKey)
	if err != nil {
		return nil, nil, err
	}

	sharedKey, err := h.getSharedKey(resp.EphemeralPublicKey)
	if err != nil {
		return nil, nil, err
	}

	// Verify response signature
	// Signature parts: "WLABS-HELLO-RESPONSE-V1", <shared ephemeral key>, <credential>
	if !peerPublicKey.VerifyParts(
		resp.Signature,
		helloResponseSignaturePrefix,
		sharedKey,
		resp.Credential,
	) {
		return nil, nil, ErrBadSignature
	}

	sessionID, err := h.getSessionID(resp.SessionId, sharedKey)
	if err != nil {
		return nil, nil, err
	}

	res := &Results{
		SessionID:  sessionID,
		NewSession: sessionID != h.config.SessionID,
		PublicKey:  peerPublicKey,
		Credential: resp.Credential,
	}

	if prevState == stateSentHello {
		resp, err := h.buildResponse(sessionID, sharedKey, nil)
		if err != nil {
			return nil, nil, err
		}
		return res, resp, nil
	} else {
		return res, nil, nil
	}
}

func (h *Handshake) buildResponse(sessionID uint64, sharedKey []byte, ephemeralPublicKey []byte) (*handshake.HelloResponse, error) {
	signature, err := h.config.PrivateKey.SignParts(helloResponseSignaturePrefix, sharedKey, h.config.Credential)
	if err != nil {
		return nil, err
	}
	return &handshake.HelloResponse{
		SessionId:          sessionID,
		EphemeralPublicKey: ephemeralPublicKey,
		Credential:         h.config.Credential,
		PublicKey:          h.config.PrivateKey.Public().MarshalRaw(),
		Signature:          signature,
	}, nil
}

func (h *Handshake) getEphemeralKey() (*keyagreement.PrivateKey, error) {
	if h.ephKey == nil {
		var err error
		h.ephKey, err = keyagreement.GenerateKey()
		if err != nil {
			return nil, err
		}
	}
	return h.ephKey, nil
}

func (h *Handshake) getSharedKey(rawPeerEphPub []byte) ([]byte, error) {
	if h.sharedKey == nil {
		peerEphPub, err := keyagreement.UnmarshalRawPublicKey(rawPeerEphPub)
		if err != nil {
			return nil, err
		}
		ephKey, err := h.getEphemeralKey()
		if err != nil {
			return nil, err
		}
		h.sharedKey = ephKey.DeriveSharedKey(peerEphPub)
	}
	return h.sharedKey, nil
}

func (h *Handshake) getSessionID(peerSessionID uint64, sharedKey []byte) (uint64, error) {
	if h.sessionID == 0 {
		resumeSessionID := h.config.SessionID
		if resumeSessionID != 0 && peerSessionID == resumeSessionID {
			h.sessionID = resumeSessionID
		} else {
			sessionIDBytes, err := keyderivation.DeriveKey(sessionIDDeriveInfo, sharedKey, 8)
			if err != nil {
				return 0, err
			}
			h.sessionID = binary.BigEndian.Uint64(sessionIDBytes)
		}
	}
	return h.sessionID, nil
}
