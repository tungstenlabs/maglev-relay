package handshake

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"maglev/internal/cryptov1/testcrypto"
)

func TestHandshakeSymmetricNewSession(t *testing.T) {
	configA := Config{
		PrivateKey: testcrypto.KeyA,
	}
	configB := Config{
		PrivateKey: testcrypto.KeyB,
	}

	handshakeA := New(configA)
	handshakeB := New(configB)

	helloA, err := handshakeA.InitiateHello()
	require.NoError(t, err)
	helloB, err := handshakeB.InitiateHello()
	require.NoError(t, err)

	respA, err := handshakeA.ProcessHello(helloB)
	require.NoError(t, err)
	respB, err := handshakeB.ProcessHello(helloA)
	require.NoError(t, err)

	resA, nilResp, err := handshakeA.ProcessHelloResponse(respB)
	require.NoError(t, err)
	assert.Nil(t, nilResp)
	resB, nilResp, err := handshakeB.ProcessHelloResponse(respA)
	require.NoError(t, err)
	assert.Nil(t, nilResp)

	assert.NotZero(t, resA.SessionID)
	assert.Equal(t, resA.SessionID, resB.SessionID)
	assert.True(t, resA.NewSession)
	assert.True(t, resB.NewSession)

	assert.Empty(t, resA.Credential)
	assert.Empty(t, resB.Credential)

	assert.True(t, resA.PublicKey.Equal(testcrypto.KeyB.Public()))
	assert.True(t, resB.PublicKey.Equal(testcrypto.KeyA.Public()))
}

func TestHandshakeAsymmetricResumeSession(t *testing.T) {
	var sessionID uint64 = 123
	configA := Config{
		PrivateKey: testcrypto.KeyA,
		SessionID:  sessionID,
		Credential: []byte("tokenA"),
	}
	configB := Config{
		PrivateKey: testcrypto.KeyB,
		SessionID:  sessionID,
		Credential: []byte("tokenB"),
	}

	handshakeA := New(configA)
	handshakeB := New(configB)

	helloA, err := handshakeA.InitiateHello()
	require.NoError(t, err)

	respB, err := handshakeB.ProcessHello(helloA)
	require.NoError(t, err)

	resA, respA, err := handshakeA.ProcessHelloResponse(respB)
	require.NoError(t, err)

	resB, nilResp, err := handshakeB.ProcessHelloResponse(respA)
	require.NoError(t, err)
	assert.Nil(t, nilResp)

	assert.Equal(t, sessionID, resA.SessionID)
	assert.Equal(t, sessionID, resB.SessionID)
	assert.False(t, resA.NewSession)
	assert.False(t, resB.NewSession)

	assert.Equal(t, []byte("tokenB"), resA.Credential)
	assert.Equal(t, []byte("tokenA"), resB.Credential)
}
