package transport

import (
	"fmt"

	"google.golang.org/protobuf/encoding/protowire"
	"google.golang.org/protobuf/proto"

	"maglev/protogen/transport"
	"maglev/statuserr"
)

// Msg binary format: <header size varint> <header> <payload>

type Msg struct {
	transport.MsgHeader
	Payload []byte
}

func UnmarshalMsg(data []byte) (*Msg, error) {
	msg := &Msg{}
	if err := msg.UnmarshalBinary(data); err != nil {
		return nil, err
	}
	return msg, nil
}

func (m *Msg) EncodeProtoPayload(payload proto.Message) error {
	rawPayload, err := proto.Marshal(payload)
	if err != nil {
		return err
	}
	m.Payload = rawPayload
	return nil
}

func (m *Msg) DecodeProtoPayload(payload proto.Message) error {
	err := proto.Unmarshal(m.Payload, payload)
	if err != nil {
		return fmt.Errorf("decode %T: %w", payload, err)
	}
	return nil
}

func (m *Msg) EncodeError(err error) {
	m.Error = statuserr.For(err).ToProto()
	m.ExchangeSeq = 0
	m.ExchangeContinues = false
	m.NoPayload = true
}

func (m *Msg) StatusError() error {
	return statuserr.FromProto(m.Error)
}

func (m *Msg) MarshalBinary() ([]byte, error) {
	headerBytes, err := proto.Marshal(&m.MsgHeader)
	if err != nil {
		return nil, fmt.Errorf("marshal header: %w", err)
	}
	out := make([]byte, 0, protowire.SizeBytes(len(headerBytes))+len(m.Payload))
	out = protowire.AppendBytes(out, headerBytes)
	out = append(out, m.Payload...)
	return out, nil
}

func (m *Msg) UnmarshalBinary(data []byte) error {
	headerBytes, n := protowire.ConsumeBytes(data)
	if err := protowire.ParseError(n); err != nil {
		return fmt.Errorf("slice header: %w", err)
	}
	if err := proto.Unmarshal(headerBytes, &m.MsgHeader); err != nil {
		return fmt.Errorf("unmarshal header: %w", err)
	}
	m.Payload = data[n:]
	return nil
}

func (m *Msg) Clone() *Msg {
	clone := &Msg{Payload: append([]byte{}, m.Payload...)}
	proto.Merge(&clone.MsgHeader, &m.MsgHeader)
	return clone
}
