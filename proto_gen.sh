#!/bin/bash

set -eu

# Change to "this" directory
cd "$(dirname "$0")"

# TODO: use e.g. submodules for this
proto_root=../maglev/protos
[ -d $proto_root ] || {
  echo "Proto root not found at ${proto_root}"
  exit 1
}

# Remove existing generated files
rm_files=(protogen/**/*.pb.go)
[[ -f ${rm_files[0]} ]] && rm $rm_files

proto_files=$(cd "${proto_root}" && echo wlabs/maglev/{handshake,transport}.proto)
set -x
protoc -I=${proto_root} \
  --go_out=. --go_opt=module=maglev \
  --go-grpc_out=. --go-grpc_opt=module=maglev \
  ${proto_files}
