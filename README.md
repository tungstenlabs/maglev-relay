# Maglev Relay

The Maglev relay provides signaling and fast-start services for the
[Maglev networking library](https://maglev.wlabs.dev/).

## Running the Relay

Prequisite: Go 1.15+

```sh
$ git clone https://gitlab.com/tungstenlabs/maglev-relay.git
$ cd maglev-relay
$ go run ./cmd/maglev-relay
```

This defaults to serving the relay from `localhost:3035`. You can override
this address with the argument `-addr=<ip:port>`.

## Public Hosting

For public hosting we currently recommended running the relay behind a reverse proxy
such as nginx. The relay uses Websockets, which may require special attention
when configuring a reverse proxy, such as with this example nginx config snippet:

```nginx
map $http_upgrade $connection_upgrade {
  default upgrade;
  '' close;
}
server {
  location / {
    proxy_pass http://localhost:3035/;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
  }
  # ... SERVER CONFIG ...
}
```
